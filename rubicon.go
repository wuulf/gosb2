package main

/*
import (
	"encoding/hex"
	"fmt"
	"strings"

	"golang.org/x/crypto/blowfish"
)

var PASSWORD = "has been removed"
var ERR_PASSWORD = "when Zarathustra was 30 years old he left his home and the lake of his home and went into the mountains"

//WARNING!! will only work for strings equal or shorter than blowfish blocksize = 8 bytes

func main_old() {

	prices := [6]string{
		"06.93308",
		"01.34821",
		"12.31345",
		"00.23913",
		"102.9000",
		"149.1341",
	}

	for _, priceString := range prices {
		dst, err1 := encrypt([]byte(PASSWORD), []byte(priceString))
		src, err2 := decrypt([]byte(PASSWORD), dst)
		if err1 != nil && err2 != nil {
			fmt.Println("Errors!")
		} else {
			fmt.Printf(
				"%v => 0x%v\n",
				string(src),
				strings.ToUpper(string(dst)))
		}
	}

	for _, priceString := range prices {
		dst, err1 := encrypt([]byte(ERR_PASSWORD), []byte(priceString))
		src, err2 := decrypt([]byte(ERR_PASSWORD), dst)
		if err1 != nil && err2 != nil {
			fmt.Println("Errors!")
		} else {
			fmt.Printf(
				"%v => 0x%v\n",
				string(src),
				strings.ToUpper(string(dst)))
		}
	}
}

func encrypt(key []byte, text []byte) ([]byte, error) {
	encryptedText, err := encryptBytes(key, text)
	if err != nil {
		return nil, err
	}
	return encodeHex(encryptedText), nil
}

func decrypt(key []byte, text []byte) ([]byte, error) {
	decodedText, err := decodeHex(text)
	if err != nil {
		return nil, err
	}
	return decryptBytes(key, decodedText)
}

//Only use it if you're sure that number of bytes is less than blocksize = 8
func encryptBytes(key []byte, bytes []byte) ([]byte, error) {
	encryptCipher, err := blowfish.NewCipher(key)
	if err != nil {
		return nil, err
	}
	encryptedBytes := make([]byte, len(bytes))
	encryptCipher.Encrypt(encryptedBytes, bytes)
	return encryptedBytes, nil
}

func encodeHex(text []byte) []byte {
	encodedText := make([]byte, hex.EncodedLen(len(text)))
	hex.Encode(encodedText, text)
	return encodedText
}

func decodeHex(text []byte) ([]byte, error) {
	decodedText := make([]byte, hex.DecodedLen(len(text)))
	_, err := hex.Decode(decodedText, text)
	if err != nil {
		return nil, err
	}
	return decodedText, nil
}

func decryptBytes(key []byte, bytes []byte) ([]byte, error) {
	decryptCipher, err := blowfish.NewCipher(key)
	if err != nil {
		return nil, err
	}
	decryptedBytes := make([]byte, len(bytes))
	decryptCipher.Decrypt(decryptedBytes, bytes)
	return decryptedBytes, nil
}*/
