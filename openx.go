// openx
package main

import (
	"crypto/hmac"
	"crypto/sha1"
	"encoding/base64"
	"encoding/binary"
	"encoding/hex"
	"errors"
	"fmt"
	"strconv"
	"strings"
	"time"
)

var IV_SIZE = 16
var CIPHERTEXT_SIZE = 8
var INTEGRITY_SIZE = 4

var ENCRIPTION_KEY = "has been removed"
var INTEGRITY_KEY = "has been removed" 
var TEST_PRICE = "2400"
var TEST_PRICE_ENCRYPTED = "AAABXWWJ0x3YUjWEnS689bRYm-NDHd-74tERBQ"

func stringsToBytes(priceString string, eKeyString string, iKeyString string) ([]byte, []byte, []byte) {
	priceLong, _ := strconv.Atoi(priceString)
	price := make([]byte, 8)
	binary.BigEndian.PutUint64(price, uint64(priceLong))
	eKey, _ := decodeHex([]byte(ENCRIPTION_KEY))
	iKey, _ := decodeHex([]byte(INTEGRITY_KEY))
	return price, eKey, iKey
}

// returns byte array of length 16 containing current time in ms followed by current time in mc
func getTimeBytes() []byte {
	//current time
	timeNano := time.Now().UnixNano()
	//current time in ms
	timeMillis := timeNano / int64(time.Millisecond)
	//current time in mc
	timeMicros := timeNano / int64(time.Microsecond)

	//convert times to byte arrays
	byteMillis := make([]byte, 8)
	byteMicros := make([]byte, 8)
	binary.BigEndian.PutUint64(byteMillis, uint64(timeMillis))
	binary.BigEndian.PutUint64(byteMicros, uint64(timeMicros))

	//make compound byte array
	return append(byteMillis, byteMicros...)
}

func encryptEncodeBytes(price []byte, eKey []byte, iKey []byte) []byte {

	// get time bytes array
	outBuff := getTimeBytes()

	//init hmac
	mac := hmac.New(sha1.New, eKey)

	// add time to hmac
	mac.Write(outBuff)

	// Now write 8 bytes of the keypad to the buffer, finishing first mac operation
	outBytes := append(outBuff, mac.Sum(nil)[:8]...)

	// Update the buffer by xor-ing the keypad values with the the
	// plaintext values.
	for i := 0; i < CIPHERTEXT_SIZE; i++ {
		outBytes[IV_SIZE+i] = outBytes[IV_SIZE+i] ^ price[i]
	}

	// Compute the signature reinitting mac operation
	mac = hmac.New(sha1.New, iKey)

	// Add price
	mac.Write(price)

	// Add key
	mac.Write(outBytes[:16])

	//Add integrity check
	outBytes = append(outBytes, mac.Sum(nil)[:4]...)

	//return encrypted byte array
	return outBytes
}

// returns encrypted string using hmac sha1
func encryptPrice(priceString string, eKeyString string, iKeyString string) string {
	price, eKey, iKey := stringsToBytes(priceString, eKeyString, iKeyString)
	encrypted := encryptEncodeBytes(price, eKey, iKey)
	encryptedString := base64.StdEncoding.EncodeToString(encrypted)
	//some wild magic of making string web-friendly
	encryptedString = strings.Replace(encryptedString, "+", "-", -1)
	encryptedString = strings.Replace(encryptedString, "/", "_", -1)
	return encryptedString
}

//Run test
func main() {
	fmt.Printf(
		"price of %v encrypted is %v \n",
		TEST_PRICE,
		encryptPrice(TEST_PRICE, ENCRIPTION_KEY, INTEGRITY_KEY))

	priceDecoded, err := decryptPrice(TEST_PRICE_ENCRYPTED, ENCRIPTION_KEY, INTEGRITY_KEY)

	if err != nil {
		fmt.Printf("Error: %v", err)
	}

	fmt.Printf(
		"encrypted in %v is the price of %v \n",
		TEST_PRICE_ENCRYPTED,
		priceDecoded)
}

// decrypts and decodes string, returns price uint64
func decryptPrice(cryptedString string, eKeyString string, iKeyString string) (uint64, error) {
	//same wild magic as above
	cryptedString = strings.Replace(cryptedString, "-", "+", -1)
	cryptedString = strings.Replace(cryptedString, "_", "/", -1) + "=="
	cryptedBytes, err := base64.StdEncoding.DecodeString(cryptedString)
	if err != nil {
		return 0, err
	}
	eKey, _ := decodeHex([]byte(ENCRIPTION_KEY))
	iKey, _ := decodeHex([]byte(INTEGRITY_KEY))

	price, err := decryptBytesToBytes(cryptedBytes, eKey, iKey)
	if err != nil {
		return 0, err
	}

	return binary.BigEndian.Uint64(price), nil
}

//decoding encrypted bytes, returns price as descrypted bytes
func decryptBytesToBytes(crypted []byte, eKey []byte, iKey []byte) ([]byte, error) {

	price := make([]byte, CIPHERTEXT_SIZE)
	mac := hmac.New(sha1.New, eKey)
	mac.Write(crypted[:IV_SIZE])
	pad := mac.Sum(nil)

	for i := 0; i < CIPHERTEXT_SIZE; i++ {
		price[i] = (pad[i] ^ crypted[IV_SIZE+i])
	}

	//Not sure if it's neccessary, but be as it may - checking signature
	mac = hmac.New(sha1.New, iKey)
	mac.Write(price)
	mac.Write(crypted[:IV_SIZE])
	sig := mac.Sum(nil)

	for i := 0; i < INTEGRITY_SIZE; i++ {
		if sig[i] != crypted[IV_SIZE+CIPHERTEXT_SIZE+i] {
			return nil, errors.New("Signature does not match")
		}
	}

	return price, nil
}

func decodeHex(text []byte) ([]byte, error) {
	decodedText := make([]byte, hex.DecodedLen(len(text)))
	_, err := hex.Decode(decodedText, text)
	if err != nil {
		return nil, err
	}
	return decodedText, nil
}
